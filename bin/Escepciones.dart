import "dart:io";

selectGrade(int grade) {
  
  if(grade < 0 || grade > 10) {
    throw 'You should enter a value between 0 and 10';
  }
  
  switch (grade) {
    case 10: //if(Grade == 'A')
      print("Excellent");
      break;
    case 9: // else if(...
      print("Good");
      break;
    case 8:
      print("OK");
      break;
    case 7:
      print("Mmmmm....");
      break;
    case 6:
      print("You must do better than this");
      break;
    default:
      print("What is your grade anyway?");
  }
}

void main() {
  String decission;
  while(true){

    print('Type your Grade [0-10]');
    int grade = int.parse(stdin.readLineSync());
    
    try {
      selectGrade(grade);
    } catch(e) {
      print(e);
    } finally {
      print('Desea Continuar [Y]:');
      decission = stdin.readLineSync();
      if(decission == 'Y' || decission == 'y') continue;
      else break;
    }
    
  }
}
