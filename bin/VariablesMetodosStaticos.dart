class Color {
  static const RED = const Color('red'); // A constant static variable.
  final String name;                     // An instance variable.
  const Color(this.name);                // A constant constructor.
}

class Counter {
  static num _i = 0;
  
  num _j;
  
  Counter(num j) {
    _i++;
    _j = j;
  }
  
  num get j => _j;
}

main() {
  var colorBlue = new Color("blue");
  colorBlue..name = 'yellow';
  
  print(Color.RED.name);
  
  print(colorBlue.name); //enums
  
  var counter1 = new Counter(3);
//  counter1._j += 5;
  
  print(Counter._i);
  print(counter1.j);
  
  var counter2 = new Counter(4);
  
  print(Counter._i);
  
  Counter._i += 5;
  
  print(Counter._i);
  
  
}