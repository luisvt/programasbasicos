import 'dart:math' hide Point;

class Point {
  num x;
  num y;
  num z;
  
  Point({this.x : 0, this.y: 0, this.z : 0});
  
  String toString() => 'point: [$x, $y, $z]';
  
  distanceTo(Point other) {
    var dx = x - other.x;
    var dy = y - other.y;
    var dz = z - other.z;
    return sqrt(dx * dx + dy * dy);
  }
}

class Rectangle {
  num left;
  num top;
  num width;
  num height;

  Rectangle(this.left, this.top, this.width, this.height);

  // Define two calculated properties: right and bottom.
  num get right             => left + width;
      set right(num value)  => left = value - width;
  num get bottom            => top + height;
      set bottom(num value) => top = value - height;
}


main() {
  var x = 3;
  var point = new Point(x: 9, y: 6);
  
  var other = new Point(x: 3, y: 4, z: 5);
  
  print(point.distanceTo(other));
  
  var rectangle = new Rectangle(0, 0, 5, 5);
  
  print(rectangle.right);
  
  rectangle.right = 8;
  
  print(rectangle.left);
  
}