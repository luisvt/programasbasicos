void main() {
  //primitive
  const double number = 42.1; //numbers, string, list, maps, object.
                    // num -> int, int8, int16, int32, int64, long, longlong
                    // num -> double float, 
  // printf("The number is %f", number)
  
//  number = 33;
  print('The number is $number'); //ctrl+r
  
  String gretting = "hello";
  
  print("$gretting world!");
  
  List<double> intList = [1, 2, 3, 4,"hello"];
  
  print("$intList");
  
  List<String> stringList = [2, 3, "hello"];
  
  print(stringList);
  
  Map<String, int> someMap = {
     "hello": 1,
     "hi": "bonjour"
  };
  
  print(someMap);
}
