main() {
  var list = [{1:"hola"},3,6];
  
  print(list);
  
  // while, do-while, for, forEach
//  var i = 3;
//  while(i < list.length) {
//    print(list[i]);
//    i += 3;
//  }
  
//  do {
//    print(list[i]);
//    i++;
//  } while(i < list.length);
  
//  for(var i = 0; i < list.length; i++) {
//    print(list[i]);
//  }
//  
//  for(var element in list) {
//    print(element);
//  }
  
  list.forEach((element) {
    //delay
    print(element);
  });
  
  print("hello");
}