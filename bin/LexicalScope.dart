//import 'funcion/funcionesInternas.dart';

var topLevel = true;

main() {
    var insideMain = true;
    
    myFunction() {
      var insideFunction = true;
      
      nestedFunction() {
        var insideNestedFunction = true;
        print('topLevel: $topLevel');
        print('insideMain: $insideFunction');
        print('insideFunction: $insideFunction');
        print('insideNestedFunction: $insideNestedFunction');
      }
//      print('insideNestedFunction: $insideNestedFunction');
      nestedFunction();
    }
    
    myFunction();
}

