abstract class Musical {
  bool canPlayPiano = false;
  bool canCompose = false;
  bool canConduct = false;
  
  void entertainMe() {
    if (canPlayPiano) {
      print('Playing piano');
    } else if (canConduct) {
      print('Waving hands');
    } else {
      print('Humming to self');
    }
  }
}

class Person {
  String firstName;
  String lastname;
  
  String sayHello() => 'hello';
//  DateTime dob;
  
}

class Maestro extends Person with Musical {
  Maestro(String maestroName) {
    firstName = maestroName;
    canConduct = true;
  }
}

main() {
  Maestro maestro = new Maestro("Luis")..canCompose = true;
  
  print('name: ${maestro.firstName}, canConduct: ${maestro.canConduct}');
  
  maestro.entertainMe();
}