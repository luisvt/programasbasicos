import 'dart:math' hide Point;

class Vector {
  final int x;
  final int y;
  const Vector(this.x, this.y);

  Vector operator +(Vector v) { // Overrides + (a + b).
    return new Vector(x + v.x, y + v.y);
  }

  Vector operator -(Vector v) { // Overrides - (a - b).
    return new Vector(x - v.x, y - v.y);
  }
  
  sum(Vector v) => new Vector(x + v.x, y + v.y);
  
  String toString() => '[$x, $y]';
}

main() {
  var vector1 = new Vector(5, 3);
  var vector2 = new Vector(6, 4);
  
  print(vector1.sum(vector2));
  
}